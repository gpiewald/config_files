## vim: ft=conf

# start counting windows at 1
set -g base-index 1

# Automatically set window title
set-window-option -g automatic-rename on
set-option -g set-titles on
set-option -g repeat-time 1000

set -g default-terminal "screen-256color"

set-option -g word-separators " -@()[]{}"

set -g mouse on
## On tmux 2.0 or below, use these instead of the above:
# set -g mouse-resize-pane   on
# set -g mouse-select-pane   on
# set -g mouse-select-window on
# set -g mouse-utf8          on


setw -g mode-keys vi

# remap prefix to Control + o
set -g prefix C-o
bind-key o send-prefix
unbind-key C-b

bind-key r source-file ~/.tmux.conf

# switch and split windows
bind-key Space next-window
bind-key BSpace previous-window
bind-key O last-window
bind-key v split-window -h
bind-key s split-window -v

# switch pane
bind-key h select-pane -L
bind-key j select-pane -D
bind-key k select-pane -U
bind-key l select-pane -R
bind-key C-o last-pane

bind-key m copy-mode
bind-key p paste-buffer

bind-key J resize-pane -D 10
bind-key K resize-pane -U 10
bind-key H resize-pane -L 10
bind-key L resize-pane -R 10

bind-key M-j resize-pane -D 2
bind-key M-k resize-pane -U 2
bind-key M-h resize-pane -L 2
bind-key M-l resize-pane -R 2

bind-key -r [ swap-pane -U # swap pane to prev position
bind-key -r ] swap-pane -D # swap pane to next position

## Window boarder design
# since version 2.9:
set -g pane-border-style        "bg=default fg=black"
set -g pane-active-border-style "bg=default fg=colour33"
# before version 2.9:
#set -g pane-border-bg black
#set -g pane-active-border-bg brightred

## Status bar design
set -g status-justify left
set -g status-bg colour17
set -g status-fg white
set -g status-interval 2

set -g status-right "#[fg=brightwhite] (#{=21:pane_title}) %H:%M %d-%b-%y"

# window status
setw -g window-status-current-format "#[fg=brightwhite]#[bg=colour33] #I#F #W "
setw -g window-status-format         "#[fg=black]#[bg=brightwhite] #I#F #W "

