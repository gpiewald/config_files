#!/bin/sh

new_user=gpi

grep '^${new_user}:' /etc/passwd >/dev/null &&
      echo 'User $new_user EXISTS' ||
      echo 'User $new_user does NOT EXIST'

echo ""
echo "Create user '$new_user' as alias to root on the target? [yn]"
read resp
if [[ $resp != "y" ]]; then
  exit 0
fi

/usr/sbin/useradd -o -u 0 -g 0 $new_user &&
       /usr/bin/passwd -d $new_user >/dev/null   || exit 1
cp ./bashrc_board    /home/$new_user/.profile    || exit 1
cp ./tmux_board.conf /home/$new_user/.tmux.conf  || exit 1
cp ./opkg.conf       /home/$new_user/opkg.conf   || exit 1
