#!/usr/bin/zsh

board_addr=${1}
new_user=gpi

if [[ $# -ne 1 ]]; then
  echo "Please provide target hostname or IP address!" >&2
  exit 1
fi

# Check if board is reachable
output=`ssh -o StrictHostKeyChecking=yes root@$board_addr 2>&1 true`
ret_val=$?
if [[ $ret_val -ne 0 ]]; then
  if echo "$output" | grep -q "REMOTE HOST IDENTIFICATION HAS CHANGED!"; then
    echo "Apparently the host key has changed. Updating it."
    target_ip=$( ssh -G $board_addr | awk '$1 == "hostname" { print $2 }' )
    ssh-keygen -f "/home/gpi/.ssh/known_hosts" -R $target_ip
    ssh-keyscan -H $target_ip >> ~/.ssh/known_hosts 2>/dev/null
  else
    echo "Failed to connect to $board_addr. Aborting" >&2
    exit 1
  fi
fi

# Print board info
ssh root@$board_addr "
    echo -en '\nBoard is reachable!\n\nHostname: ' && hostname &&
    ( grep '^${new_user}:' /etc/passwd >/dev/null &&
      echo 'User $new_user EXISTS' ||
      echo 'User $new_user does NOT EXIST'
    )" || exit 1

echo ""
echo -n "Create user '$new_user' as alias to root on the target? [yn] "
read resp
if [[ $resp != "y" ]]; then
  exit 0
fi

ssh root@$board_addr "/usr/sbin/useradd -o -u 0 -g 0 $new_user &&
                      /usr/bin/passwd -d $new_user >/dev/null"
scp ${0:A:h}/bashrc_board    ${new_user}@${board_addr}:.profile
scp ${0:A:h}/tmux_board.conf ${new_user}@${board_addr}:.tmux.conf
scp ${0:A:h}/opkg.conf       ${new_user}@${board_addr}:opkg.conf
