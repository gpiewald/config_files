# vim: ft=sh fdm=marker fdc=2

PS1='\[\033[1;31m\]\u\[\033[0;31m\]@\h \[\e[0;90m\]\w\[\e[1;37m\]\[\e[m\]\$ '
#       light-red   |    dark-red    |   dark-gray  |   white     |  dflt
#                   |                |              |             `prompt
#                   |                |              |
#                   |                |              |
#                   |                |              `path
#                   |                `hostname
#                   `username

LS_COLORS='no=00:fi=00:di=01;34:ln=01;36:pi=40;33:so=01;35:bd=40;33;01:cd=40;33;01:ex=01;32:mi=5;31;46:or=5;31;46:*.cmd=01;32:*.exe=01;32:*.com=01;32:*.btm=01;32:*.bat=01;32:*.deb=01;31:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.jpg=01;35:*.gif=01;35:*.bmp=01;35:*.xbm=01;35:*.xpm=01;35:*.png=01;35:*.ppm=01;35:'

alias ls="ls --human-readable --classify --color=auto --time-style=long-iso"
alias ll="ls -l"
alias lsa='ls -A'          # show hidden files
alias date="date +'%Y-%m-%d %H:%M:%S'"
alias ..="cd .."
alias ...="cd ../../"

alias psg="ps -Af | grep -i"
