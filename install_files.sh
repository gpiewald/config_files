#!/usr/bin/zsh

autoload -Uz colors && colors

link_file()
{
   local link=~/$1
   local target=`pwd`/$2
   local resp="o"

   echo -e "$fg[yellow]Creating link $link -> $target $reset_color"

   if [[ -L $link ]]; then
      # File already exists and is a link
      local current_target=`ls -l $link | awk '{print $NF}'`
      if [[ $current_target == $target ]]; then
         # The link is already correctly set. Don't do anything.
         echo -n "Already exists.. "
         resp="s"
      else
         echo "The file '$link' alrady exists and is link pointing to '$current_target'."
         echo "Do you want to [o]verwrite it with the correct link, [s]kip or [a]bort?"
         read -k -s resp
      fi
   elif [[ -d $link ]]; then
      # File already exists and is a directory
      echo "The file alrady exists and is a DIRECTORY. Do you want to [d]elete it and create a link, [s]kip or [a]bort?"
      read -k -s resp
      if [[ $resp == d ]]; then
         rm -rf $link
         resp="o"
      fi
   elif [[ -f $link ]]; then
      # File already exists and is a regular file
      echo "The file alrady exists and is NOT a link. Do you want to [v]iew a diff, [o]verwrite, [s]kip or [a]bort?"
      read -k -s resp
      while [[ $resp == v ]] ; do
         vimdiff "$link" "$target"
         echo "Do you want to [v]iew a diff, [o]verwrite, [s]kip or [a]bort?"
         read -k -s resp
      done
   fi

   case $resp in
      o) # Create the link
         ln --force -s $target $link
         ;;
      s) echo Skipping
         ;;
      a) echo Aborting
         exit 0
         ;;
      *) echo Aborting
         exit 1
         ;;
   esac
}

create_vim_folders()
{
   echo "Do you want to create a .vim folder hierarchy and install all VIM plugins? [yn]"
   read -k -s resp
   if [[ $resp != y ]]; then
      return
   fi
   test -d ~/.vim || mkdir ~/.vim
   test -d ~/.vim/swp || mkdir ~/.vim/swp

   # Install Vundle and all plugins manages with Vundle
   if [[ -e ~/.vim/bundle/vundle ]]; then
      (cd ~/.vim/bundle/vundle
      git pull)
   else
      git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/vundle
   fi
   vim +PluginInstall +PluginUpdate +qall

   # The taglist plugin is not available on GitHub (at least not in the latest
   # version), so we install it manually.
   # Downloaded from https://www.vim.org/scripts/script.php?script_id=273
   test -f ~/.vim/plugin/taglist.vim || unzip -d ~/.vim vim_plugins/taglist_46.zip
}

link_file .screenrc screenrc_main
link_file .vimrc  vimrc
create_vim_folders
link_file .zshrc  zshrc
link_file .zsh    _zsh
link_file .bashrc bashrc
link_file .lynxrc lynxrc
link_file .tmux.conf tmux.conf
link_file .gitconfig gitconfig
link_file .gitignore gitignore
test -d ~/.subversion && link_file .subversion/config subversion_config

