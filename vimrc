"" vim: ft=vim fdm=marker tw=80 fdc=2

" Options
"{{{
scriptencoding utf-8
"set clipboard=exclude:.*         " avoid long startup delay
set nocompatible                 " be iMproved
set history=200                  " Nr of lines to remember in the history table
set background=light             " assume terminal background is light, so use dark foreground colours
set hidden                       " allow changing a buffer even without saving. This also preserves the undo-history.
set hlsearch                     " highlight search pattern
set incsearch                    " update screen while typing search pattern
set ignorecase                   " search case insensitively
set smartcase                    " override ignorcase when search pattern contains upper case letters
set shiftwidth=2                 " nr of spaces for indentation
set tabstop=8                    " nr of spaces for displaying a real tap, and for retabbing
set expandtab                    " spaces instead of tabs
set smartindent                  " be smart when indenting lines
set nowrap                       " don't wrap long lines
set wildmenu                     " enhanced command-line completion
set wildmode=longest,list        " settings for enhanced command-line completion
set whichwrap=<,>,h,l,[,]        " advance cursor to next line on these movements
set backspace=indent,eol,start   " allow backspace over indent, eol and start of line
set nobackup                     " don't make backup files
set directory=~/.vim/swp//
set noautowrite                  " don't write when switching the buffer
set laststatus=2                 " always put a status line in each window
set cmdheight=1                  " cmd line is one char high
set modeline                     " allow file-specific vim-settings (vim: tw=80 ...)
set modelines=4                  " check 4 lines for modelines
set report=1                     " display a msg if more than 1 line changed
set tildeop                      " tilde behaves like an operator
set visualbell
set nojoinspaces                 " when joining lines (J, gq), do not put a double space after .!?
set errorfile=errors.vim
set spellfile=$HOME/.vim/spell/en.utf-8.add
set listchars=tab:»·,trail:·,precedes:<,extends:>
"set listchars=tab:>-,trail:*,precedes:<,extends:>
set list
set statusline=%f%r%m\ %=\ [%l,%c]\ %p%%\ of\ %L
set grepprg=grep\ -nH\ $*
"}}}


" Colours
"{{{
syntax on
"hi SpellBad   ctermfg=Darkred   ctermbg=NONE
hi Folded     ctermfg=DarkGray  ctermbg=NONE
hi FoldColumn ctermfg=DarkGray  ctermbg=NONE
hi StatusLine ctermfg=DarkGray  ctermbg=White
"hi DiffText   ctermbg=9 ctermfg=black cterm=none  gui=bold guibg=Red guifg=Black
hi DiffText ctermbg=224

"" When using vimdiff or diff mode
"highlight DiffAdd    term=bold         ctermbg=darkgreen ctermfg=white    cterm=none guibg=DarkGreen  guifg=White    gui=bold
"highlight DiffText   term=reverse,bold ctermbg=red       ctermfg=yellow   cterm=none guibg=DarkRed    guifg=yellow   gui=bold
"highlight DiffChange term=bold         ctermbg=black     ctermfg=white    cterm=none guibg=Black      guifg=White    gui=bold
"highlight DiffDelete term=none         ctermbg=darkblue  ctermfg=darkblue cterm=none guibg=DarkBlue   guifg=DarkBlue gui=none
"if &background == "light"
"	" Changes when bg=white fg=black
"	highlight DiffAdd                   ctermfg=black cterm=none guibg=green      guifg=black
"	highlight DiffText   ctermbg=yellow ctermfg=red   cterm=none guibg=yellow     guifg=red
"	highlight DiffChange ctermbg=none   ctermfg=none  cterm=none guibg=white      guifg=black
"	highlight DiffDelete                                         guibg=lightblue  guifg=lightblue
"endif
"
"" When viewing a diff or patch file
"highlight diffRemoved term=bold ctermbg=black   ctermfg=red    cterm=none guibg=DarkRed     guifg=white gui=none
"highlight diffAdded   term=bold ctermbg=black   ctermfg=green  cterm=none guibg=DarkGreen   guifg=white gui=none
"highlight diffChanged term=bold ctermbg=black   ctermfg=yellow cterm=none guibg=DarkYellow  guifg=white gui=none
"highlight diffLine    term=bold ctermbg=magenta ctermfg=white  cterm=none guibg=DarkMagenta guifg=white gui=none
"highlight diffFile    term=bold ctermbg=yellow  ctermfg=black  cterm=none guibg=DarkYellow  guifg=white gui=none
"if &background == "light"
"	" Changes when bg=white fg=black
"	highlight diffRemoved ctermbg=black   ctermfg=red    cterm=none guibg=Red     guifg=black
"	highlight diffAdded   ctermbg=black   ctermfg=green  cterm=none guibg=Green   guifg=black
"	highlight diffChanged ctermbg=black   ctermfg=yellow cterm=none guibg=Yellow  guifg=black
"	highlight diffLine    ctermbg=magenta ctermfg=white  cterm=none guibg=Magenta guifg=black
"	highlight diffFile    ctermbg=yellow  ctermfg=black  cterm=none guibg=Yellow  guifg=black
"endif

"}}}


" Vundle
"{{{
"
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install(update) bundles
" :BundleSearch(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
" NOTE: comments after Bundle command are not allowed..

filetype off                   " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#begin()

" let Vundle manage Vundle (required!)
Bundle 'gmarik/vundle'

"" My Bundles here:
""
"" original repos on github
Plugin 'taglist.vim'
Plugin 'YankRing.vim'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'IndexedSearch'
Plugin 'bufexplorer.zip'
"Plugin 'c.vim'
"Plugin 'bash-support.vim'
"Plugin 'hexman.vim'
Plugin 'gnupg.vim'
Plugin 'vcscommand.vim'
"Plugin 'csv.vim'
Plugin 'https://github.com/kergoth/vim-bitbake.git'
Plugin 'Directory-specific-settings'
Plugin 'lightdiff'
Plugin 'linediff.vim'

call vundle#end()
filetype plugin indent on     " required!
"}}}


" Plugin settings
" {{{
let g:VCSCommandMapPrefix = "<Leader>v"
let g:tex_flavor='latex'

" YankRing
let g:yankring_max_history = 20
let g:yankring_min_element_length = 2
let g:yankring_max_element_length = 4096
let g:yankring_map_dot = 1    " this is default
let g:yankring_history_dir = '~/.vim'
function! YRRunAfterMaps()
   nnoremap Y   :<C-U>YRYankCount 'y$'<CR>
endfunction
nnoremap <silent> <leader>yr :YRShow<CR>

" Taglist
highlight MyTagListTagName  ctermbg=green ctermfg=black
set updatetime=1500
let g:Tlist_WinWidth = 35
let g:Tlist_Auto_Open = 1
let g:Tlist_Exit_OnlyWindow = 1
let Tlist_File_Fold_Auto_Close = 1
"let Tlist_GainFocus_On_ToggleOpen = 0
"let g:Tlist_Show_One_File = 1
"let g:Tlist_Enable_Fold_Column = 0

" LaTeX suite
" remap the jump function, so that my <C-j> mapping is not overwritten
nmap <C-ä>   <plug>IMAP_JumpForward
imap <C-ä>   <plug>IMAP_JumpForward
vmap <C-ä>   <plug>IMAP_JumpForward
" }}}


" Various mappings
" {{{
nnoremap Y y$
nnoremap <C-q> :qa<cr>
nnoremap <C-s> :w<cr>
nnoremap <C-x> :bd<cr>
nnoremap <C-i> <C-]>
nnoremap <F1>  :nohlsearch<cr>
nnoremap <F2>  :set list! list?<cr>
nnoremap <F3>  :tabp<cr>
nnoremap <F4>  :tabn<cr>
nnoremap <F5>  :checktime<cr>
nnoremap <F6>  :set paste! paste?<cr>
nnoremap <F7>  :set spell! spell?<cr>
nnoremap <F8>  :TlistToggle<cr>

nnoremap  ,cd :cd %:p:h<CR>:pwd<CR>

" easy moving between buffers and windows
nnoremap <C-b> :b#<cr>
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-h> <C-W>h
nnoremap <C-l> <C-W>l

" scrolling
nnoremap <right> 8zl
nnoremap <left>  8zh
nnoremap <up>    5<C-y>
nnoremap <down>  5<C-e>

" folds
nnoremap <space> za
nnoremap zh zczO
nnoremap zs [z
nnoremap ze ]z

" swap words, push word left/right
nnoremap <silent> gw "_yiw:s/\(\%#\w\+\)\(\_W\+\)\(\w\+\)/\3\2\1/<CR><c-o><c-l>:nohl<CR>
nnoremap <silent> gl "_yiw?\w\+\_W\+\%#<CR>:s/\(\%#\w\+\)\(\_W\+\)\(\w\+\)/\3\2\1/<CR><c-o><c-l>:nohl<CR>
nnoremap <silent> gr "_yiw:s/\(\%#\w\+\)\(\_W\+\)\(\w\+\)/\3\2\1/<CR><c-o>/\w\+\_W\+<CR><c-l>:nohl<CR>

" paste a previously yanked selection over the word under cursor (in normal
" mode) or current selection (in visual mode):
vnoremap ö "0p
nnoremap ö viw"0p

" Insert Mode Mappings
inoremap <C-k> <up>
inoremap <C-j> <down>
inoremap <C-h> <left>
inoremap <C-l> <right>
inoremap <C-f> <Esc>gUiw`]a
inoremap <C-c> <Esc>bgUlea

" Command Mode Mappings
cnoremap <C-A>      <Home>
cnoremap <C-E>      <End>
cnoremap <C-B>      <Left>
cnoremap <C-F>      <Right>
cnoremap <C-N>      <Down>
cnoremap <C-P>      <Up>
cnoremap <ESC>b     <S-Left>
cnoremap <ESC>f     <S-Right>
cnoremap <C-D>      <Del>

" Abbreviations
iabbr teh the
iabbr latexx LaTeX
"}}}


" Tab Line
" {{{
function! ShortTabLine()
  let ret = ''
  for i in range(tabpagenr('$'))
    " select the color group for highlighting active tab
    if i + 1 == tabpagenr()
      let ret .= '%#errorMsg#'
    else
      let ret .= '%#TabLine#'
    endif
    " find the buffername for the tablabel
    let buflist = tabpagebuflist(i+1)
    let winnr = tabpagewinnr(i+1)
    let buffername = bufname(buflist[winnr - 1])
    let filename = fnamemodify(buffername,':t')
    " check if there is no name
    if filename == ''
      let filename = 'noname'
    endif
    " only show the first 6 letters of the name and
    " .. if the filename is more than 8 letters long
    if strlen(filename) >=12
      let ret .= ' '. filename[0:10].'.. '
    else
      let ret .= ' '.filename.' '
    endif
  endfor
  " after the last tab fill with TabLineFill and reset tab page #
  let ret .= '%#TabLineFill#%T'
  return ret
endfunction

set tabline=%!ShortTabLine()
"}}}


" Autocommands
"{{{
augroup autocommands
   "autocmd! * " Remove ALL autocommands for the current group.
   autocmd BufRead *.c,*.h,*.cpp,*.cc,*.hpp   :call MyFileTypeSettings_C()
   autocmd BufRead *.xml                      :call MyFileTypeSettings_xml()
   autocmd BufRead *.sh                       :call MyFileTypeSettings_sh()
   autocmd BufRead *.lst                      set filetype=sh
   autocmd BufRead *.sa                       set filetype=asm
   autocmd BufRead *.service,*.target         set filetype=systemd
   autocmd BufRead Makefile*,MakePkg,*.tmpl   set filetype=make
   autocmd BufRead toolselect                 set filetype=SH
   autocmd BufRead Jenkinsfile                set filetype=groovy
augroup END
"}}}


" MyFileTypeSettings_C
"{{{
if !exists("*MyFileTypeSettings_C")
function! MyFileTypeSettings_C()
   setlocal syntax=c.doxygen
   setlocal foldmethod=syntax
   setlocal foldcolumn=3
   setlocal cindent
   setlocal cinoptions=t0,(0
   setlocal smarttab
   setlocal formatoptions=croql
   setlocal comments=sr:/*,mb:*,elx:*/,://,fb:-
   setlocal cinwords=if,else,while,do,for,switch,case

   hi Todo term=reverse cterm=bold ctermfg=white ctermbg=red
   hi doxygenEmphasisedWord cterm=bold,underline
   hi doxygenHtmlItalic cterm=bold
   let doxygen_javadoc_autobrief = 0

   inoremap <F7> // TODO(gpi)

   " C-Support plugin
   let g:C_Ctrl_j   = 'off'
endfunction
endif
"}}}


" MyFileTypeSettings_sh
"{{{
if !exists("*MyFileTypeSettings_sh")
function! MyFileTypeSettings_sh()
   let g:sh_fold_enabled = 1
   set foldmethod=syntax
endfunction
endif
"}}}


" MyFileTypeSettings_xml
"{{{
if !exists("*MyFileTypeSettings_xml")
function! MyFileTypeSettings_xml()
   let g:xml_syntax_folding=1
   setlocal foldmethod=syntax
endfunction
endif
"}}}


" Diff
"{{{
   " This diff function uses "-w" instead of "-b", to ignore *all* whitespace
   " changes (not only non-leading whitespace)
   "set diffexpr=MyDiff()
   if !exists("*MyDiff")
   function MyDiff()
      let opt = ""
      if &diffopt =~ "icase"
        let opt = opt . "-i "
      endif
      if &diffopt =~ "iwhite"
        let opt = opt . "-w "
      endif
      silent execute "!diff -a --binary " . opt . v:fname_in . " " . v:fname_new .
      \  " > " . v:fname_out
   endfunction
   endif
"}}}


" My tips
"{{{
augroup mytips
   autocmd BufWrite mytips.txt   :helptags ~/.vim/doc/
   autocmd BufRead  mytips.txt   set filetype=help
   autocmd BufRead  mytips.txt   set noreadonly
   autocmd BufRead  mytips.txt   set modifiable
augroup END
nnoremap <leader>m :tabnew ~/.vim/doc/mytips.txt<CR>
"}}}
