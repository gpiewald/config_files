local opts = { noremap = true, silent = false }
local keymap = vim.keymap.set


-- Various
keymap('n', '<C-q>', vim.cmd.qa, opts)
keymap('n', '<C-s>', vim.cmd.w, opts)
keymap('n', '<C-x>', vim.cmd.bd, opts)
keymap('n', '<C-i>', '<C-]>', opts)
keymap('n', '<F1>', vim.cmd.nohlsearch, opts)
keymap('n', '<F2>', ':set list!<CR>:set list?<CR>', opts)
keymap('n', '<F3>', vim.cmd.tabp, opts)
keymap('n', '<F4>', vim.cmd.tabn, opts)
keymap('n', '<F5>', vim.cmd.checktime, opts)
keymap('n', '<F6>', ':set paste!<CR>:set paste?<CR>', opts)
keymap('n', '<F7>', ':set spell!<CR>:set spell?<CR>', opts)
keymap('n', '<right>', '8zl', opts)
keymap('n', '<left>',  '8zh', opts)
keymap('n', '<up>',    '5<C-y>', opts)
keymap('n', '<down>',  '5<C-e>', opts)

keymap('v', 'ö', '"0p', opts)
keymap('n', 'ö', 'viw"0p', opts)

-- Folds
keymap('n', '<space>',  'za', opts)
keymap('n', 'zh',  'zczO', opts)
keymap('n', 'zs',  '[z', opts)
keymap('n', 'ze',  ']z', opts)

-- Insert Mode Mappings
keymap('i', '<C-k>', '<up>', opts)
keymap('i', '<C-j>', '<down>', opts)
keymap('i', '<C-h>', '<left>', opts)
keymap('i', '<C-l>', '<right>', opts)
keymap('i', '<C-f>', '<Esc>gUiw`]a', opts)
keymap('i', '<C-c>', '<Esc>bgUlea', opts)

-- Easy moving between buffers and windows
keymap('n', '<C-j>', '<C-W>j', opts)
keymap('n', '<C-k>', '<C-W>k', opts)
keymap('n', '<C-h>', '<C-W>h', opts)
keymap('n', '<C-l>', '<C-W>l', opts)

keymap("n", "<S-l>", ":tabnext<CR>", opts)
keymap("n", "<S-h>", ":tabprevious<CR>", opts)

-- Resize with arrows
keymap("n", "<C-Up>", ":resize +2<CR>", opts)
keymap("n", "<C-Down>", ":resize -2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

