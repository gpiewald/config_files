require "gpi.options"
require "gpi.keymaps"



local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git", "clone", "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", lazypath })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
  --{ "folke/tokyonight.nvim", lazy = false, priority = 1000, opts = {} },
  { "rmehri01/onenord.nvim", branch = "main" },
  { "nvim-telescope/telescope.nvim", tag = "0.1.5", dependencies = { "nvim-lua/plenary.nvim" } },
  { "nvim-treesitter/nvim-treesitter", build = ":TSUpdate" },
  { "andrewradev/linediff.vim" },
  { "vim-scripts/vcscommand.vim" },
  { "chrisbra/Colorizer" },
  { "j-morano/buffer_manager.nvim" },
  { "VonHeikemen/lsp-zero.nvim", branch = "v3.x"},
  { "neovim/nvim-lspconfig"},
  { "hrsh7th/cmp-nvim-lsp"},
  { "hrsh7th/nvim-cmp"},
  { "L3MON4D3/LuaSnip"},
}
local opts = {}
require("lazy").setup(plugins, opts)

-- Color scheme
--vim.cmd.colorscheme "tokyonight-day"
vim.cmd.colorscheme "onenord"
require('onenord').setup({ theme = "light"})

-- Telescope
local telescope = require('telescope.builtin')
vim.keymap.set('n', '<leader>tf', telescope.find_files, {})
vim.keymap.set('n', '<leader>tg', telescope.git_files, {})
vim.keymap.set('n', '<leader>ts', telescope.live_grep, {})
vim.keymap.set('n', '<leader>tb', telescope.buffers, {})
vim.keymap.set('n', '<leader>th', telescope.help_tags, {})

-- Treesitter
local configs = require("nvim-treesitter.configs")
configs.setup({
    ensure_installed = { "c", "cpp", "lua", "vim", "vimdoc", "python", "groovy" },
    auto_install = true,
    highlight = { enable = true },
    indent = { enable = true },  
    additional_vim_regex_highlighting = false,
  })

-- Language Server
local lsp_zero = require('lsp-zero')
lsp_zero.on_attach(function(client, bufnr)
  lsp_zero.default_keymaps({buffer = bufnr})
  vim.keymap.set('n', '<leader>th', telescope.help_tags, {})
  vim.keymap.set('n', '<leader>R', '<cmd>lua vim.lsp.buf.rename()<cr>', {buffer = bufnr})
end)
require('lspconfig').clangd.setup({})
require'lspconfig'.pylsp.setup{
  settings = {
    pylsp = {
      plugins = {
        pycodestyle = { ignore = {'W391'}, maxLineLength = 100 }
      }
    }
  }
}

local cmp = require('cmp')
cmp.setup({
  mapping = cmp.mapping.preset.insert({
    ['<tab>'] = cmp.mapping.confirm({select = true}),

    -- scroll up and down the documentation window
    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
    ['<C-d>'] = cmp.mapping.scroll_docs(4),   
  })
})
