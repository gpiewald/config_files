## vim: fdm=marker tw=80 fdc=3


#{{{ Terminal Color Escape Sequences
Default=$'\e[0m'
Black=$'\e[0;30m'
Blue=$'\e[0;34m'
Green=$'\e[0;32m'
Cyan=$'\e[0;36m'
Red=$'\e[0;31m'
Purple=$'\e[0;35m'
Brown=$'\e[0;33m'
LightGray=$'\e[0;37m'
DarkGray=$'\e[1;30m'
LightBlue=$'\e[1;34m'
LightGreen=$'\e[1;32m'
LightCyan=$'\e[1;36m'
LightRed=$'\e[1;31m'
LightPurple=$'\e[1;35m'
Yellow=$'\e[1;33m'
White=$'\e[1;37m'
FgYelBgBlue=$'\e[01;44;33m'
#}}}

#{{{ Environment Variables

## ZLE
zle_highlight=(region:standout special:standout isearch:underline)

## LOCALE
export LANG=en_US.UTF-8
export LC_ALL="en_US.UTF-8"

# PATH
# Note: $path always contains the same as $PATH, but as an array, which is
#       easier to handle. Same with $FPATH.
typeset -U path      # Prevent adding multiple times to $path if this file is sourced again
path=(~/bin /sbin $path)

typeset -U fpath
fpath=(~/.zsh $fpath)

## HISTORY
export HISTFILE=$HOME/.zsh_history
export HISTSIZE=8192
export SAVEHIST=8192

# PAGER (less)
export PAGER=/usr/bin/less
export LESS_TERMCAP_mb=${LightRed}    # start blinking mode
export LESS_TERMCAP_md=${LightRed}    # start bold mode
export LESS_TERMCAP_me=${Default}     # end all modes like so, us, mb, md and mr
export LESS_TERMCAP_so=${FgYelBgBlue} # start standout mode
export LESS_TERMCAP_se=${Default}     # end standout mode
export LESS_TERMCAP_us=${LightGreen}  # start underlining
export LESS_TERMCAP_ue=${Default}     # end underlining
#export LESSOPEN='|/usr/bin/lesspipe %s'  # specify a tool to preprocess the input (e.g. uncompress a *.tar.gz to list the contained files).

## MISC
export EDITOR=vim
#export HTTP_PROXY='http://proxy.frequentis.frq:8080'
#export http_proxy=$HTTP_PROXY
#export GPG_TTY='tty'      # this is sometimes needed for gpg. Not quite sure when...
#}}}

#{{{ Aliases
alias tmux='tmux -2'                      # Force the use of 256 colors in tmux

alias cb='cd ~1'                          # jump to the last directory
alias cbb='cd ~2'                         # jump to the second last directory
alias dirs='dirs -v'                      # print directory stack
alias cp='cp -a'                          # copy in archive mode (recursive, links, attributes)
alias df='df -h'
alias psa='ps -A'
alias grep='grep --color=auto'
alias vim='vim -p'                        # open several files always as tabs
alias nvim='nvim -p'                        # open several files always as tabs
alias sourcezshrc='source ~/.zshrc'
alias tree='tree -C'
alias cal='cal -m'
alias dus='du -hs'
alias du1='du -h --max-depth=1'
alias du2='du -h --max-depth=2'
alias date='date "+%F %T"'

## global aliases
alias -g ...='../..'
alias -g ....='../../..'
alias -g .....='../../../..'

## aptitude
alias ash='aptitude show'
alias ase='aptitude search'
alias ainst='sudo aptitude install'

## ls
export LS_COLORS='no=00:fi=00:di=01;34:ln=01;36:pi=40;33:so=01;35:bd=40;33;01:cd=40;33;01:ex=00;32:mi=5;31;46:or=5;31;46:*.cmd=01;32:*.exe=01;32:*.com=01;32:*.btm=01;32:*.bat=01;32:*.deb=01;31:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.jpg=01;35:*.gif=01;35:*.bmp=01;35:*.xbm=01;35:*.xpm=01;35:*.png=01;35:*.ppm=01;35:'
#export FIGNORE="~:.o"    # List of file endings to be ignored on a ls
alias ls="ls --human-readable --classify --color=auto --time-style=long-iso"
alias lsd="ls -d"
alias ll="ls -l"
alias lld="lsd -ld"
alias l1="ls -1"
alias lsa='ls -A'          # show hidden files
alias lsx='ls -X'          # sort by extension
alias lsbig='ls -Slh'
alias lssmall='ls -Slhr'
alias lsnew="ls -tlr | tail"
alias lsold="ls -tl  | tail"

alias mylatexmk="latexmk -quiet -g -pdf -pdflatex='pdflatex -interaction=nonstopmode' -output-directory=out"

## docker
alias Dps="docker ps -a --format 'table {{.ID}}\t{{.Names}}\t{{.Status}}\t{{.Image}}'"
alias Dpss="docker ps -a --format 'table {{.ID}}\t{{.Names}}\t{{.Status}}\t{{.Size}}'"
alias Dim="docker images"
#}}}

#{{{ Options
export REPORTTIME=8
#setopt auto_cd
setopt auto_menu        # use menu completion after the second request for completion
setopt autopushd        # push every visited directory on directory stack (dirs -v)
setopt pushd_minus      # "cd -<tab>" shows the dir stack in reverse (more intuitive) order
#setopt auto_resume
setopt brace_ccl        # {a-e}  expands to 'a b c d e'
setopt complete_in_word # completion is done from both ends
setopt correct          # try to correct the spelling of commands
setopt extended_glob    # treat ‘#’, ‘~’ and ‘^’ as patterns for filename generation
setopt hash_list_all    # before completion, make sure the entire command path is hashed first
setopt list_types       # in completion menu show file-type with a trailing mark
#setopt mail_warning
setopt no_clobber       # don't overwrite existing files with '>':
setopt no_flow_control
setopt no_hup
setopt no_notify
setopt printexitvalue
setopt pushd_ignoredups
setopt pushd_silent
setopt rm_star_wait     # wait 10 sec for confirmation before executing "rm *"
setopt c_bases          # output hex values like 0xAB, instead of 16#AB

## History
#setopt append_history      # append the local history to $HISTFILE at shell exit
#setopt share_history       # like inc_append_history, plus always import $HISTFILE
setopt inc_append_history   # append each command immediately to $HISTFILE
#setopt hist_ignore_dups    # don't push a command if the previous was the same
setopt hist_ignore_all_dups # don't push a command if any previous was the same
setopt hist_ignore_space    # don't push a command that starts with a blank character
#}}}

#{{{ User-defined widgets

#{{{2 Complete with dots
expand-or-complete-with-dots()
{
  echo -n "\e[31m......\e[0m"
  zle expand-or-complete
  zle redisplay
}
zle -N expand-or-complete-with-dots
#}}}2

#{{{2 Insert SVN URL
insert-svn-url() {
   svn info >& /dev/null
   if [[ $? == 0 ]]; then
      zle -U `svn info | grep '^URL:' | cut -c 6-`
   else
      echo -ne '\a'
   fi
}
zle -N insert-svn-url
#}}}2

#{{{2 Get SVN modified files
get-svn-modified() {
   # Reorder all modified files of the current working directory into a flat,
   # space-separated list and add them to the command line.
   zle -U "`svn stat 2> /dev/null | awk 'BEGIN {list=""}; /^M/ {list=list $2 " "}; END {print list}'`"
}
zle -N get-svn-modified
#}}}2

#{{{2 Get GIT modified files
get-git-modified() {
   # Reorder all modified files of the current working directory into a flat,
   # space-separated list and add them to the command line.
   zle -U "`git status -s 2> /dev/null | awk 'BEGIN {list=""}; /^ M/ {list=list $2 " "}; END {print list}'`"
}
zle -N get-git-modified
#}}}2

#{{{2 VCS status
vcs-status() {
   setopt no_autopushd
   cmd=""
   cur_dir=`pwd`
   while [[ `pwd` != "/" ]]; do
      if [[ -d .svn ]]; then
         cmd="svn status"
         break
      elif [[ -d .git ]]; then
         cmd="git status -s"
         break
      fi
      cd ..
   done
   cd $cur_dir
   if [[ -n "$cmd" ]]; then
      zle -U "$cmd"
   fi
   setopt autopushd
}
zle -N vcs-status
#}}}2

#{{{2 Insert sudo
insert_sudo ()
{
   zle beginning-of-line; zle -U "sudo "
}
zle -N insert-sudo insert_sudo
#}}}2

#{{{2 Magic abbreviation expand
typeset -Ag abbreviations
abbreviations=(
  "Im"    "| more"
  "Ig"    "| grep"
  "Ip"    "| $PAGER"
  "Ih"    "| head"
  "Ik"    "| keep"
  "It"    "| tail"
  "Is"    "| sort"
  "Iw"    "| wc"
  "Ix"    "| xargs"
  "Ia"    "| awk '{}'"
  "Ie"    "| sed ''"
  "CM"    "CMakeLists.txt"
)

magic-abbrev-expand() {
    local MATCH
    LBUFFER=${LBUFFER%%(#m)[_a-zA-Z0-9]#}
    LBUFFER+=${abbreviations[$MATCH]:-$MATCH}
    zle self-insert
}
zle -N magic-abbrev-expand
#}}}2

#}}}

#{{{ Functions

#{{{2 tidybuild
tidybuild()
{
  dir=${PWD:t}
  if [[ $dir =~ 'build*' ]]; then
    setopt no_autopushd
    cd .. && rm -rf $dir && mkdir $dir && cd $dir
    setopt autopushd
  else
    echo "Error you're not in a directory named 'build*'"
  fi
}
#}}}2
#
#{{{2 mandelbrot
mandelbrot()
{
   local lines columns colour a b p q i pnew
   ((columns=COLUMNS-1, lines=LINES-1, colour=0))
   for ((b=-1.5; b<=1.5; b+=3.0/lines)) do
      for ((a=-2.0; a<=1; a+=3.0/columns)) do
         for ((p=0.0, q=0.0, i=0; p*p+q*q < 4 && i < 32; i++)) do
            ((pnew=p*p-q*q+a, q=2*p*q+b, p=pnew))
         done
         ((colour=(i/4)%8))
         echo -n "\\e[4${colour}m "
      done
      echo
   done
}
#}}}2

#{{{2 eps2pdf
eps2pdf()
{
   for arg;
   do
       echo "$arg => ${arg/eps/pdf}";
       epstopdf $arg;
   done
}
#}}}2

#}}}

#{{{ Key bindings
# keymap selection
bindkey -e                                      # Emacs keymap
# string bindings
bindkey "\eOP"  vcs-status                      # F1:       "svn status" or "git status"
bindkey -s "\e[23~" "svn stat -u^M"             # Shift-F1: svn stat -u
bindkey -s "\e[17~" "**/*"                      # F6:       **/*
bindkey -s "\e[18~" "**/*.(c|cc|cpp|h|hh|hpp)"  # F7:       **/*.(c|cpp|h)

# user-defined widgets
bindkey "^I"  expand-or-complete-with-dots
bindkey "^[u" insert-svn-url                    # M-u insert URL of current SVN repository
bindkey "^[m" get-git-modified                  # M-m insert list of modified files in current working dir
bindkey "^[s" insert-sudo                       # M-s insert sudo
bindkey " "   magic-abbrev-expand               # "Ig" -> "| grep"

# builtin widgets
bindkey '^t' transpose-words
bindkey '^j' vi-match-bracket
bindkey '^o' overwrite-mode
bindkey '^q' copy-prev-word
WORDCHARS='*?_-.[]~=/&;!#$%^(){}<>:'
bindkey "^[b" backward-word
bindkey "^[B" vi-backward-word
bindkey "^[f" forward-word
bindkey "^[F" vi-forward-word
bindkey "^X^X" vi-repeat-find
bindkey "^X^Y" vi-rev-repeat-find
#}}}

#{{{ Autoloads
autoload -Uz colors && colors
autoload -U zcalc
autoload -U zmv
autoload run-help

autoload -z edit-command-line       # edit the command line in a normal editor
zle -N edit-command-line
bindkey "^X^E" edit-command-line
#}}}

#{{{ Completion
autoload -U compinit && compinit
zstyle ':completion:*' completer _oldlist _expand _complete _correct _approximate _force_rehash
zstyle ':completion:*' verbose yes
zstyle ':completion:*' group-name ''
zstyle ':completion:*:descriptions' format ${White}%U%d%u${Default}
zstyle ':completion:*:corrections'  format $'%S%d (errors: %e)%s'
zstyle ':completion:*:messages'     format $'%S%d%s'
zstyle ':completion:*:warnings'     format $'%S%d%s'
zstyle ':completion:*'              menu select=2
zstyle ':completion:*:default'      list-colors ${(s.:.)LS_COLORS} ma=0\;42
zstyle ':completion:*:approximate:*' max-errors 1 numeric
zstyle ':completion:*:-tilde-:*'    tag-order named-directories   # complete hashed directories, but not usernames
zstyle -e ':completion::*:hosts'     hosts 'reply=($(sed -e "/^#/d" -e "s/ .*\$//" -e "s/,/ /g" /etc/ssh_known_hosts(N) ~/.ssh/known_hosts(N) 2>/dev/null | xargs) $(grep \^Host ~/.ssh/config(N) | cut -f2 -d\  2>/dev/null | xargs))'

_force_rehash() {
  (( CURRENT == 1 )) && rehash
  return 1
}
#}}}

#{{{ Prompt, vcs_info
function use_vcs_info() {
  if [[ $1 == "no" ]]; then
    precmd() { }
    vcs_info_msg_0_=""
    unset -f precmd
    PS1='%F{red}%B%n%b%F{red}@%m:%f%~%(?|%B%#%b|%F{red}%#%f) '
    #              |           |    |       |           `prompt (if $? != 0)
    #              |           |    |       `prompt (if $? == 0)
    #              |           |    |
    #              |           |    `path
    #              |           `hostname
    #              `username
  else
    precmd() { vcs_info }
    PS1='%F{red}%B%n%b%F{red}@%m:%f%~${vcs_info_msg_0_}%(?|%B%#%b|%F{red}%#%f) '
    #              |           |    |        |                |           `prompt (if $? != 0)
    #              |           |    |        |                `prompt (if $? == 0)
    #              |           |    |        `vcs_info
    #              |           |    `path
    #              |           `hostname
    #              `username
  fi
}

autoload -Uz vcs_info
if type vcs_info > /dev/null; then
   # vcs_info is available
   setopt prompt_subst     # parameter expansion in the prompt
   zstyle ':vcs_info:*' enable git svn
   zstyle ':vcs_info:*' formats       "%F{cyan}[%b%c%u]%f"
   zstyle ':vcs_info:*' branchformat  '%b'
   zstyle ':vcs_info:*' actionformats '%F{cyan}[%b|%a%c%u]%f '
   zstyle ':vcs_info:*' stagedstr     ' ●'
   zstyle ':vcs_info:*' unstagedstr   ' ○'
   zstyle ':vcs_info:*' check-for-changes true
   zstyle ':vcs_info:git*+set-message:*' hooks git-abbrv-branch
   function +vi-git-abbrv-branch() {
     # do the following replacements:
     #   "feature/xxx" -> "f/xxx"
     #   "bugfix/xxx"  -> "b/xxx"
     #   "hotfix/xxx"  -> "h/xxx"
     # then truncate the name to 20 chars
     hook_com[branch]="${hook_com[branch]/feature\//f/}"
     hook_com[branch]="${hook_com[branch]/bugfix\//b/}"
     hook_com[branch]="${hook_com[branch]/hotfix\//h/}"
     if [[ ${#hook_com[branch]} -gt 20 ]]; then
       hook_com[branch]="${hook_com[branch]:0:20}…"
     fi
   }

   use_vcs_info yes
else
   use_vcs_info no
fi

PS2='%_> '
RPS1="%(?..%F{red}[%?]%f)"

#}}}

#{{{ Machine local settings
if [[ -f ~/config_files/zshrc.local ]]; then
   source ~/config_files/zshrc.local
fi
#}}}

# Switch off scroll lock, so I can use CTRL-S and CTRL-Q in applications
stty -ixon
stty -ixoff

